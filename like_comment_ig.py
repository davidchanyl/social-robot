
import os
import time
import requests

from selenium import webdriver
import mysql.connector

def automate_comment(browser, content):
    comment_xpath_str = "//textarea[contains(@placeholder, '留言⋯')]"
    comment_el = browser.find_element_by_xpath(comment_xpath_str)
    if comment_el == None:
        print("None")
        comment_el = browser.find_element_by_xpath(comment_xpath_str)
    try:
        comment_el.send_keys(content)  
    except:
        comment_el = browser.find_element_by_xpath(comment_xpath_str)
        comment_el.send_keys(content) 
        
    submit_btns_xpath = "button[type='submit']"
    submit_btns_els = browser.find_elements_by_css_selector(submit_btns_xpath)
    time.sleep(5)
    for btn in submit_btns_els:
        try:
            btn.click()
        except:
            pass

def automate_likes(browser):
    like_heart_svg_xpath = "//*[contains(@aria-label, '讚')]"
    all_like_hearts_elements = browser.find_elements_by_xpath(like_heart_svg_xpath)
    max_heart_h = -1
    for heart_el in all_like_hearts_elements:
        h = heart_el.get_attribute("height")
        current_h = int(h)
        if current_h > max_heart_h:
            max_heart_h = current_h
    all_like_hearts_elements = browser.find_elements_by_xpath(like_heart_svg_xpath)
    for heart_el in all_like_hearts_elements:
        h = heart_el.get_attribute("height")
        if h == max_heart_h or h == f"{max_heart_h}":
            parent_button = heart_el.find_element_by_xpath('..')
            time.sleep(5)
            try:
                parent_button.click()
            except:
                pass


mydb = mysql.connector.connect(
  host="lfmerukkeiac5y5w.cbetxkdyhwsb.us-east-1.rds.amazonaws.com",
  user="mtbbxoypd0210xsf",
  password="yujwx790xco0exh8",
  database="m73ihuf5eebkpfji"
)

mycursor = mydb.cursor()

mycursor.execute("select ig_email, ig_password, like_ig_id, comment from like_and_comment where status is null")

myresult = mycursor.fetchall()

for row in myresult:
    ig_email = row[0]
    ig_password = row[1]
    like_ig_id = row[2]
    post_comment = row[3]
    print ("ig_email: ", ig_email, "ig_password: ", ig_password, " like_ig_id:", like_ig_id, " post_comment:", post_comment)
    browser = webdriver.Chrome()
    url = "https://www.instagram.com"
    browser.get(url)
    time.sleep(2)
    username_el = browser.find_element_by_name("username")
    username_el.send_keys(ig_email)
    password_el = browser.find_element_by_name("password")
    password_el.send_keys(ig_password)
    time.sleep(2)
    submit_btn_el = browser.find_element_by_css_selector("button[type='submit']")
    submit_btn_el.click()
    time.sleep(4)
    the_rock_url = "https://www.instagram.com/" + like_ig_id
    print(the_rock_url)
    browser.get(the_rock_url)
    post_url_pattern = "https://www.instagram.com/p/<post-slug-id>"
    post_xpath_str = "//a[contains(@href, '/p/')]"
    post_links = browser.find_elements_by_xpath(post_xpath_str)
    post_link_el = None
    if len(post_links) > 0:
        post_link_el = post_links[0]
    if post_link_el != None:
        post_href = post_link_el.get_attribute("href")
        browser.get(post_href)
    content = post_comment    
    automate_comment(browser, content)
    automate_likes(browser)
    browser.close()

update_sql = "update like_and_comment set status = 'Completed' where status is null"
mycursor.execute(update_sql)
mydb.commit()
print(mycursor.rowcount, "record(s) affected")
mycursor.close()
mydb.close()



