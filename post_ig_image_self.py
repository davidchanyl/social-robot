import os
import time
import requests

import pyautogui
#import autoit

from selenium import webdriver
from selenium.webdriver.support.ui import Select

from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options

import mysql.connector
import random

#from fake_useragent import UserAgent

#options = Options()
#ua = UserAgent()
#userAgent = ua.random
#print(userAgent)
#options.add_argument(f'user-agent={userAgent}')
#browser = webdriver.Chrome(options=options)

chrome_options = webdriver.ChromeOptions()
#chrome_options.add_argument('--user-agent="Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 640 XL LTE) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Mobile Safari/537.36 Edge/12.10166"')

#chrome_options = Options()
chrome_options.add_argument('--user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1')

#useragent = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Mobile Safari/537.36"
#chrome_options.add_argument(f"user-agent={useragent}")

#user_agent = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16"

#profile = webdriver.FirefoxProfile() 
#profile.set_preference("general.useragent.override", user_agent)


#test_list = [3, 60, 90, 120]
test_list = [3, 5, 10, 20, 30, 60]

""" mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="1234ccCC",
  database="m73ihuf5eebkpfji"
) """

""" mydb = mysql.connector.connect(
  host="lfmerukkeiac5y5w.cbetxkdyhwsb.us-east-1.rds.amazonaws.com",
  user="mtbbxoypd0210xsf",
  password="yujwx790xco0exh8",
  database="m73ihuf5eebkpfji"
) """

mydb = mysql.connector.connect(
  host="172.105.118.220",
  user="socialapp",
  password="lCcKr8dvcJ!efaHQ",
  database="socialapp"
)

mycursor = mydb.cursor()

mycursor.execute("select ig_id, ig_password, post_image_path, post_comment, id from post_ig_image_comment where status is null")
#mycursor.execute("select ig_id, ig_password, post_image_path, post_comment, id from post_ig_image_comment where ig_id = 'ada070180' and status is null")

myresult = mycursor.fetchall()

for row in myresult:
    ig_id = row[0]
    ig_password = row[1]
    post_image_path = row[2]
    post_comment = row[3]
    post_id = row[4]
    print ("ig_id: ", ig_id, " ig_password: ", ig_password, " post_image_path:", post_image_path, " post_comment:", post_comment)    
    browser = webdriver.Chrome(options=chrome_options)
    #browser = webdriver.Firefox(profile)
    #browser.set_window_size(360,640)

    url = "https://www.instagram.com"
    browser.get(url)
    time.sleep(2)
    login_btn_el = browser.find_element_by_xpath('//button[text()="登入"]')
    login_btn_el.click()
    time.sleep(2)
    fullName_el = browser.find_element_by_name("username")
    fullName_el.send_keys(ig_id)
    password_el = browser.find_element_by_name("password")
    password_el.send_keys(ig_password)
    submit_btn_el = browser.find_element_by_css_selector("button[type='submit']")
    submit_btn_el.click()
    time.sleep(8)
    try:
        ban_el = browser.find_element_by_xpath('//*[text()="你的帳號已暫時鎖定"]')
        print('Lock:', ig_id)
        break
    except:
        pass

    try:
        later_btn_el = browser.find_element_by_xpath('//button[text()="稍後再說"]')
        later_btn_el.click()
        time.sleep(3)
    except:
        pass    
    try:
        cancel_btn_el = browser.find_element_by_xpath('//button[text()="取消"]')
        cancel_btn_el.click()
        time.sleep(3)
    except:
        pass

    try:    
        new_post_xpath = "//*[contains(@data-testid, 'new-post-button')]"
        new_post_el = browser.find_element_by_xpath(new_post_xpath)
        new_post_el.click()
        #time.sleep(3)
        time.sleep(8)
        pyautogui.write(post_image_path) 
        pyautogui.press('enter')
        time.sleep(3)
        continue_btn_el = browser.find_element_by_xpath('//button[text()="繼續"]')
        continue_btn_el.click()
        time.sleep(3)
        comment_xpath_str = "//textarea[contains(@placeholder, '輸入說明文字')]"
        #time.sleep(15)
        comment_el = browser.find_element_by_xpath(comment_xpath_str)
        comment_el.send_keys(post_comment)  
        share_btn_el = browser.find_element_by_xpath('//button[text()="分享"]')
        share_btn_el.click()
        random_num = random.choice(test_list)
        print (random_num)
        time.sleep(2)        
        update_sql = "update post_ig_image_comment set status = 'Completed', update_date = sysdate() where status is null and id = " + str(post_id)
        mycursor.execute(update_sql)
        update_sql = "update ig_account_detail set reset_date=sysdate() where ig_id = '" + ig_id + "'"
        print(update_sql)
        mycursor.execute(update_sql)
        mydb.commit()    
        time.sleep(random_num)
        browser.close()    
        
    except:
        print("error")
        break           

print(mycursor.rowcount, "record(s) affected")
mycursor.close()
mydb.close()