
import os
import time
import requests

from selenium import webdriver
import mysql.connector
import random

def automate_comment(browser, content):
    comment_xpath_str = "//textarea[contains(@placeholder, '留言⋯')]"
    comment_el = browser.find_element_by_xpath(comment_xpath_str)

    if comment_el == None:
        print("None")
        comment_el = browser.find_element_by_xpath(comment_xpath_str)
    try:
        comment_el.send_keys(content)  
    except:
        comment_el = browser.find_element_by_xpath(comment_xpath_str)
        comment_el.send_keys(content) 
        
    submit_btns_xpath = "button[type='submit']"
    submit_btns_el = browser.find_element_by_css_selector(submit_btns_xpath)
    #time.sleep(5)
    submit_btns_el.click()

def automate_likes(browser):
    like_heart_svg_xpath = "//*[contains(@aria-label, '讚')]"
    all_like_hearts_elements = browser.find_elements_by_xpath(like_heart_svg_xpath)
    max_heart_h = -1
    for heart_el in all_like_hearts_elements:
        print("heart height:",  heart_el.get_attribute("height"))
        h = heart_el.get_attribute("height")
        try:
            current_h = int(h)
        except:
            pass    
        if current_h > max_heart_h:
            max_heart_h = current_h
    all_like_hearts_elements = browser.find_elements_by_xpath(like_heart_svg_xpath)
    for heart_el in all_like_hearts_elements:
        h = heart_el.get_attribute("height")
        if h == max_heart_h or h == f"{max_heart_h}":
            print("2 heart height:",  heart_el.get_attribute("height"))
            parent_button = heart_el.find_element_by_xpath('..')
            parent_button.click()
            time.sleep(5)
            break
""" mydb = mysql.connector.connect(
  host="lfmerukkeiac5y5w.cbetxkdyhwsb.us-east-1.rds.amazonaws.com",
  user="mtbbxoypd0210xsf",
  password="yujwx790xco0exh8",
  database="m73ihuf5eebkpfji"
) """
mydb = mysql.connector.connect(
  host="172.105.118.220",
  user="socialapp",
  password="lCcKr8dvcJ!efaHQ",
  database="socialapp"
)
#work test_list = [3, 30, 60]
test_list = [3, 5, 10, 20, 30, 60]
mycursor = mydb.cursor()

#mycursor.execute("select ig_id, ig_password, like_ig_id, comment, id from like_and_comment where status is null")
mycursor.execute("select distinct ig_id, ig_password from like_and_comment where status is null")
myresult = mycursor.fetchall()

for row in myresult:
    ig_id = row[0]
    ig_password = row[1]
    #like_ig_id = row[2]
    #post_comment = row[3]
    #id = row[4]
    print ("ig_id: ", ig_id, "ig_password: ", ig_password)
    browser = webdriver.Chrome()
    url = "https://www.instagram.com"
    browser.get(url)
    time.sleep(2)
    username_el = browser.find_element_by_name("username")
    username_el.send_keys(ig_id)
    password_el = browser.find_element_by_name("password")
    password_el.send_keys(ig_password)
    time.sleep(2)
    submit_btn_el = browser.find_element_by_css_selector("button[type='submit']")
    submit_btn_el.click()
    time.sleep(8)

    try:
        ban_el = browser.find_element_by_xpath('//*[text()="你的帳號已暫時鎖定"]')
        print('Lock:', ig_id)
        break
    except:
        pass

    mycursor.execute("select ig_id, ig_password, like_ig_id, comment, id from like_and_comment where ig_id = '" + ig_id + "' and status is null")
    myresult = mycursor.fetchall()
    for row in myresult:
        like_ig_id = row[2]
        post_comment = row[3]
        id = row[4]
        print ("ig_id: ", ig_id, "ig_password: ", ig_password, " like_ig_id:", like_ig_id, " post_comment:", post_comment, " id:", id)
        
        the_rock_url = "https://www.instagram.com/" + like_ig_id
        print(the_rock_url)
        browser.get(the_rock_url)
        time.sleep(4)
        post_url_pattern = "https://www.instagram.com/p/<post-slug-id>"
        post_xpath_str = "//a[contains(@href, '/p/')]"
        post_links = browser.find_elements_by_xpath(post_xpath_str)
        post_link_el = None
        if len(post_links) > 0:
            post_link_el = post_links[0]
        if post_link_el != None:
            post_href = post_link_el.get_attribute("href")
            browser.get(post_href)
        content = post_comment    
        try:
            time.sleep(3)
            automate_comment(browser, content)
        except:
            print("comment error")
            break
        try:
            automate_likes(browser)
        except:
            print("like error")
            break
        
        
        update_sql = "update like_and_comment set status = 'Completed', update_date = sysdate() where status is null and id = " + str(id)
        mycursor.execute(update_sql)
        update_sql = "update parameters set reset_date=sysdate(),  ig_like_comment_count_self=ig_like_comment_count_self+1 where ig_id = '" + ig_id + "'"
        print(update_sql)    
        mycursor.execute(update_sql)    
        mydb.commit()
        random_num = random.choice(test_list)
        print (random_num)
        time.sleep(random_num)
    browser.close()
print(mycursor.rowcount, "record(s) affected")
mycursor.close()
mydb.close()



