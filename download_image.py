from selenium import webdriver
from bs4 import BeautifulSoup
import requests
import urllib.request
import time
import sys
import os

import mysql.connector

#taking user input
#print("What do you want to download?")
#download = input()

def download_image(download, path):
    print (download)
    site = 'https://www.google.com/search?tbm=isch&q='+download
    #providing driver path
    #driver = webdriver.Firefox()
    driver = webdriver.Chrome()
    #passing site url
    driver.get(site)
    SAVE_FOLDER = "c:/images/"+path+"/"
    if not os.path.exists(SAVE_FOLDER):
            os.mkdir(SAVE_FOLDER)
    #if you just want to download 10-15 images then skip the while loop and just write
    #driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")
    #below while loop scrolls the webpage 7 times(if available)
    i = 0
    while i<1:  
        #for scrolling page
        driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")
        try:
            #for clicking show more results button
            driver.find_element_by_xpath("/html/body/div[2]/c-wiz/div[3]/div[1]/div/div/div/div/div[5]/input").click()
        except Exception as e:
            pass
        time.sleep(5)
        i+=1
        #parsing
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    #closing web browser
    driver.close()
    #scraping image urls with the help of image tag and class used for images
    img_tags = soup.find_all("img", class_="rg_i")
    count = 0
    for i in img_tags:
        #print(i['src'])
        try:
            #passing image urls one by one and downloading
            urllib.request.urlretrieve(i['src'], SAVE_FOLDER+str(count)+".jpg")
            count+=1
            print("Number of images downloaded = "+str(count),end='\r')
        except Exception as e:
            pass

mydb = mysql.connector.connect(
  host="lfmerukkeiac5y5w.cbetxkdyhwsb.us-east-1.rds.amazonaws.com",
  user="mtbbxoypd0210xsf",
  password="yujwx790xco0exh8",
  database="m73ihuf5eebkpfji"
)

mycursor = mydb.cursor()

mycursor.execute("SELECT topic, path FROM ig_topic")

myresult = mycursor.fetchall()

for row in myresult:
    download = row[0]
    path = row[1]
    download_image(download, path)

mycursor.close()
mydb.close()

