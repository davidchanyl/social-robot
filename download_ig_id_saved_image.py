# import libraries 
import os
import time
import requests
import urllib.request
import sys
from bs4 import BeautifulSoup
from selenium import webdriver
DRIVER_PATH = '/path/chromedriver'


def download_image(url, count, destination = 'D:/images/'):
    import urllib
    
    resource = urllib.request.urlopen(url)
    #filename = destination+url[-8:]+".jpg"
    filename = destination+str(count)+".jpg"
    print(filename)
    output = open(filename,"wb")
    output.write(resource.read())
    output.close()

driver = webdriver.Chrome()

url = 'http://instagram.com/'

driver.get(url)
time.sleep(2)
username_el = driver.find_element_by_name("username")
"""username_el.send_keys("ada070180")"""
username_el.send_keys(sys.argv[1])
password_el = driver.find_element_by_name("password")
"""password_el.send_keys("aDERfly@1ab")"""
password_el.send_keys(sys.argv[2])

time.sleep(2)
submit_btn_el = driver.find_element_by_css_selector("button[type='submit']")
submit_btn_el.click()
time.sleep(4)

posts = []

scroll_time = 15
ig_id = str(sys.argv[3])
""" ig_id = str(sys.argv[1]) """

"""driver.get("https://www.instagram.com/" + ig_id)"""
driver.get("https://www.instagram.com/" + ig_id + "/saved/")

scrollCount = 0
for x in range(1,scroll_time+1):
    #scroll
    scrolldown=driver.execute_script("window.scrollTo(0, document.body.scrollHeight);var scrolldown=document.body.scrollHeight;return scrolldown;")
    match=False
    while(match==False):
        last_count = scrolldown
        scrollCount = scrollCount + 1
        time.sleep(1)
        scrolldown = driver.execute_script("window.scrollTo(0, document.body.scrollHeight);var scrolldown=document.body.scrollHeight;return scrolldown;")
        time.sleep(3)
        if last_count==scrolldown:
            match=True
        if scrollCount==x*2:
            match=True
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        for a in soup.find_all('a', href=True):
            if a['href'].startswith('/p') and not a['href'].startswith('/' + ig_id):
                if "https://www.instagram.com"+a['href'] not in posts: 
                    print("https://www.instagram.com"+a['href'])
                    posts.append("https://www.instagram.com"+a['href'])
                #print("Found the URL:", "https://www.instagram.com"+a['href'])
    print(posts)


#get videos and images
count = 0
video_count = 0
download_url = ''
for post in posts:	
    driver.get(post)
    #shortcode = driver.current_url.split("/")[-2]
    time.sleep(1)
    try:
        download_url = driver.find_element_by_css_selector("video[type='video/mp4']").get_attribute('src')
        print("video:", download_url)
        video_count = video_count + 1
        try:
            #urllib.request.urlretrieve( download_url, 'D:/videos/' + str(sys.argv[1]) + '{}.mp4'.format(str(video_count)))
            time.sleep(0)
        except IndexError as e:
            print(e)
            pass                 
    except:
        try:
            count = count + 1
            download_url = driver.find_element_by_css_selector("img[style='object-fit: cover;']").get_attribute('src')
            print("images:", download_url)
            urllib.request.urlretrieve( download_url, 'D:/images/' + ig_id + '/{}.jpg'.format(str(count)))
            time.sleep(1)        
        except IndexError as e:
            print(e)
            pass


driver.quit()
"""     if driver.find_element_by_css_selector("img[style='object-fit: cover;']") is not None:
        download_url = driver.find_element_by_css_selector("img[style='object-fit: cover;']").get_attribute('src')
        #urllib.request.urlretrieve( download_url, 'D:/images/{}.jpg'.format(shortcode))
        urllib.request.urlretrieve( download_url, 'D:/images/{}.jpg'.format(str(count)))
    else:
        download_url = driver.find_element_by_css_selector("video[type='video/mp4']").get_attribute('src')
        print("video:", download_url)
        urllib.request.urlretrieve( download_url, 'D:/images/{}.mp4'.format(str(count)))
        #urllib.request.urlretrieve( download_url, 'D:/images/{}.mp4'.format(shortcode)) """
    


#driver.quit()
""" soup = BeautifulSoup(driver.page_source)
image_link = []
for a in soup.find_all('a', href=True):
    if a['href'].startswith('/p'):
        image_link.append("https://www.instagram.com"+a['href'])
        print("Found the URL:", "https://www.instagram.com"+a['href'])

count = 0
for i,j in enumerate(image_link):    
    #driver = webdriver.Chrome()
    driver.get(j)
    soup_i = BeautifulSoup(driver.page_source, "html.parser")
    try:
        down = soup_i.find_all('div',{"class":"eLAPa kPFhm"})[0].find_all('img')[0].get('src')
        count = count + 1
        #print(count)
        download_image(down, count)
    except:
        pass
    time.sleep(1)    
    #driver_i.quit()
driver.quit()     """